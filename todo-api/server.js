const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');

const config = require('./config');

const user = require('./app/user');
const task = require('./app/task');

const app = express();

app.use(express.json());
app.use(cors());

const port = 8000;

mongoose.connect(config.dbUrl, config.mongoOptions).then(() => {
    app.use('/user', user);
    app.use('/task', task);

    app.listen(port, () => {
        console.log(`Server started on ${port} port`);
    });
});