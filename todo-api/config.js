module.exports = {
    dbUrl: 'mongodb://localhost/todoList',
    mongoOptions: {
        useNewUrlParser: true,
        useCreateIndex: true
    }
};
