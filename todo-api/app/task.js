const express = require('express');
const Task = require('../models/Task');
const auth = require('../middleware/auth');


const router = express.Router();

router.post('/', auth, (req, res) => {
    const task = new Task(req.body);
    task.user = req.user._id;

    task.save()
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error))
});

router.get('/', auth, async (req, res) => {
    Task.find().populate('user', 'username')
        .then(task => res.send(task))
        .catch(() => res.sendStatus(500))
});

router.put('/:id', auth, async (req, res) => {
    const id = req.params.id;
    const dataTask = req.body;
    if(dataTask.user) {
        delete dataTask.user
    }
    const task = await Task.findOne({_id: id});

    if (!(req.user._id).equals(task.user)) {
        res.sendStatus(403);
    } else {
        task.set({title: dataTask.title, description: dataTask.description, status: dataTask.status});
        task.save()
            .then(result => res.send(result));
    }
});

router.delete('/:id', auth, (req, res) => {
    Task.findByIdAndDelete(req.params.id)
        .then(res.send('task deleted'))
        .catch(() => res.sendStatus(500));
});

module.exports = router;